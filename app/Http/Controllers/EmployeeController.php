<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Salaries;
use App\Titles;
use http\Env\Response;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    //--------Get Employee All Details------
    public function getEmployeeDetails()
    {
        $employee = Employee::all();
        return response()->json(['http_status' => 'success', 'data' => $employee]);
    }

    //--------Get Selected Employee Details By ID for employee profile page
    public function getSelectedEmployees(Request $request)
    {
        $emp_no = $request->input('emp_no');
        $employee = Employee::with('salaries', 'titles')->where('emp_no', '=', $emp_no)->first();
        return response()->json(['http_status' => 'success', 'data' => $employee]);
    }

    //---------- Create / Update employee-------------
    public function cudEmployee(Request $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $gender = $request->input('gender');
        $hire_date = $request->input('hire_date');
        $birth_date = $request->input('birth_date');
        $emp_no = $request->input('emp_no');
        $action = $request->input('action');
        $salaries = $request->input('salaries');
        $titles = $request->input('titles');
        //checking if the employee action is delete
        switch ($action) {
            case 'update' :
                $employee = Employee::find($emp_no);
                break;
            case 'insert' :
                $employee = new Employee();
                break;
            default  :
                return response()->json(['http_status' => 'error', 'message' => 'invalid request!'], 400);
        }


        $request->validate([
            'first_name' => 'required|max:14',
            'last_name' => 'required|max:16',
            'hire_date' => 'required|date',
            'birth_date' => 'required|date',
            'gender' => 'in:male,female',
        ]);


        $employee->first_name = $first_name;
        $employee->last_name = $last_name;
        $employee->birth_date = $birth_date;
        $employee->gender = $gender;
        $employee->hire_date = $hire_date;
        if ($employee->save()) {
            if ($action == 'insert') {
                $salaries = json_decode($salaries);
                if ($salaries && is_array($salaries)) {
                    foreach ($salaries as $get_salary) {
                        $salary = new Salaries();
                        $salary->emp_no = $employee->emp_no;
                        $salary->salary = $get_salary->amount;
                        $salary->from_date = $get_salary->from_date;
                        $salary->to_date = $get_salary->to_date;
                        $salary->save();
                    }
                }

                $titles = json_decode($titles);
                if ($titles && is_array($titles)) {
                    foreach ($titles as $get_title) {
                        $title = new Titles();
                        $title->emp_no = $employee->emp_no;
                        $title->title = $get_title->title;
                        $title->from_date = $get_title->from_date;
                        $title->to_date = $get_title->to_date;
                        $title->save();
                    }
                }
            }
            return response()->json(['http_status' => 'success', 'message' => 'employee updated successfully']);
        }
    }

    //----------Delete salaries,titles and employees------
    public function deleteData(Request $request)
    {
        $type = $request->input('type');
        $id = $request->input('id');


        switch ($type) {
            case 'salary' :
                Salaries::where('id', '=', $id)->delete();
                return response()->json(['http_status' => 'success', 'message' => 'salary details deleted successfully']);
                break;
            case 'designation' :
                Titles::where('id', '=', $id)->delete();
                return response()->json(['http_status' => 'success', 'message' => 'designation details deleted successfully']);
                break;
            case 'employee' :
                Employee::where('emp_no', '=', $id)->delete();
                Salaries::where('emp_no', '=', $id)->delete();
                Titles::where('emp_no', '=', $id)->delete();
                return response()->json(['http_status' => 'success', 'message' => 'Employee deleted successfully']);
                break;
            default :
                return response()->json(['http_status' => 'error', 'message' => 'Invalid delete type']);
        }


    }
}
