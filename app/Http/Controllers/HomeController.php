<?php

namespace App\Http\Controllers;

use App\Mail\TwoFactorGeneration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('index');
    }

    public function sendMessege(Request $request){
        $name = $request->input('contact-name');
        $phone = $request->input('contact-phone');
        $email= $request->input('contact-email');
        $subject = $request->input('subject');
        $messege = $request->input('contact-message');

        $text = "Name : ".$name." <br /> phone ".$phone." <br /> email".$email." <br/> subject".$subject." <br /> ------------ messege".$messege;

        Mail::to(['tayonu@gmail.com'])->send(new TwoFactorGeneration($text));
    }

}
