<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
    public $timestamps = false;
    protected $primaryKey = 'emp_no';


    public function salaries(){
        return $this->hasMany('App\Salaries','emp_no','emp_no');
    }

    public function titles(){
        return $this->hasMany('App\Titles','emp_no','emp_no');
    }
}
