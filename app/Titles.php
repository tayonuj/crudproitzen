<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titles extends Model
{
    protected $table = 'titles';
    public $timestamps = false;

}
