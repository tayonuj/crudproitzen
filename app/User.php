<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable , HasApiTokens;
    use SoftDeletes;
    const ADMIN_TYPE = 'admin';
    const SUBUSER_TYPE = 'sub_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // -----------------------middleware admin---------------------------------------------------------
    public function isAdmin(){
        return $this->role === self::ADMIN_TYPE;
    }
    // -----------------------middleware sub user---------------------------------------------------------
    public function isSubuser(){
        return $this->role === self::SUBUSER_TYPE;
    }

    public function locations(){
        return $this->hasMany('App\UserHasLocations');
    }
    public function devices(){
        return $this->hasOne('App\Devices','user_id','id');
    }
    public function farmers(){
        return $this->hasMany('App\Farmer','created_by','id');
    }
    public function agentPoints(){
        return $this->hasMany('App\AgentTrackingPoint','user_id','id');
    }
}
