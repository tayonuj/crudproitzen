<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


//--------Get Employee All Details------
Route::post('/employee/getAllEmployees', [
    'uses' => 'EmployeeController@getEmployeeDetails'
]);
//--------Get Selected Employee Details By ID for employee profile page
Route::post('/employee/getSelectedEmployees', [
    'uses' => 'EmployeeController@getSelectedEmployees'
]);
//---------- Create / Update employee-------------
Route::post('/employee/cudEmployee', [
    'uses' => 'EmployeeController@cudEmployee'
]);
//----------Delete salaries,titles and employees------
Route::post('/employee/deleteData', [
    'uses' => 'EmployeeController@deleteData'
]);

