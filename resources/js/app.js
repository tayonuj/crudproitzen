require('./bootstrap');
window.Vue = Vue;
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'; // Ensure you are using css-loader
import '@mdi/font/css/materialdesignicons.css';// Ensure you are using css-loader
import 'vuetify/dist/vuetify.min.css';
import router from './router';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueSplide from '@splidejs/vue-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import ImgInputer from 'vue-img-inputer';
import 'vue-img-inputer/dist/index.css';
import VueGoogleCharts from 'vue-google-charts'
import * as geolib from 'geolib';
import VueConfirmDialog from 'vue-confirm-dialog'
import * as VueWindow from '@hscmap/vue-window'
import axios from 'axios';
import VueAxios from 'vue-axios';
import moment from 'moment'

Vue.use(Vuetify);
Vue.use(VueSplide);
Vue.component('ImgInputer', ImgInputer);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBKwT3-cq00IaM04TcHh1UiePAgjbp9LN4',
        libraries: 'drawing,language=si,geometry,places,visualization',
    },
});
Vue.use(VueGoogleCharts)
Vue.use(geolib);
Vue.use(VueAxios,axios);
Vue.use(VueConfirmDialog)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)
Vue.use(require('@hscmap/vue-window'))
Vue.prototype.moment = moment;

//toster
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 5000});


//vForms
import { Form, } from 'vform';
window.Form = Form;


const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(),
});
