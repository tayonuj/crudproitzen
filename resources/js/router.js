import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
import Index from "./components/Index";
import AddNewEmployee from "./components/AddNewEmployee";
import EditEmployee from "./components/EditEmployee";

const routes = [
    {
        path: "/",
        component: Index
    },
    {
        path: "/AddNewEmployee",
        component: AddNewEmployee
    },
    {
        path: "/EmployeeProfile/:id",
        component: EditEmployee
    },
];

export default new Router({
    mode: "history",
    routes
});
