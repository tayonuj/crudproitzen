-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 22, 2021 at 04:00 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agrobizz`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_layers`
--

DROP TABLE IF EXISTS `custom_layers`;
CREATE TABLE IF NOT EXISTS `custom_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bottom_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bottom_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_layers`
--

INSERT INTO `custom_layers` (`id`, `name`, `top_lat`, `top_lng`, `bottom_lat`, `bottom_lng`, `url`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'layer1', '7.010059999867032', '80.2105042213307', '6.869647382083834', '80.0237366432057', 'storage/img/customLayers/mjoAjqWTbipTMUv1p1dPd5Pqx0Bom2haWLPWmnKF.jpg', '145', '2021-01-02 17:53:08', '2021-01-02 17:53:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_drawing_layers`
--

DROP TABLE IF EXISTS `map_drawing_layers`;
CREATE TABLE IF NOT EXISTS `map_drawing_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `layer_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `map_drawing_layers`
--

INSERT INTO `map_drawing_layers` (`id`, `name`, `url`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `layer_type`) VALUES
(1, 'test1', '../storage/mapLayers/test1.json', '145', '2021-01-21 15:16:56', '2021-01-21 15:16:56', NULL, 'polyline'),
(2, 'layer 3', '../storage/mapLayers/layer 3.json', '145', '2021-01-21 15:18:35', '2021-01-21 15:18:35', NULL, 'polyline'),
(3, 'layer 1', '../storage/mapLayers/layer 1.json', '145', '2021-01-21 21:29:58', '2021-01-21 21:29:58', NULL, 'polyline'),
(4, 'polygon layerset 1', '../storage/mapLayers/polygon layerset 1.json', '145', '2021-01-21 22:13:34', '2021-01-21 22:13:34', NULL, 'polygon'),
(5, 'aaaa', '../storage/mapLayers/aaaa.json', '145', '2021-01-21 22:19:24', '2021-01-21 22:19:24', NULL, 'polygon'),
(6, 'polygon layer 4', '../storage/mapLayers/polygon layer 4.json', '145', '2021-01-21 22:25:33', '2021-01-21 22:25:33', NULL, 'polygon'),
(7, 'circleLayer 1', '../storage/mapLayers/circleLayer 1.json', '145', '2021-01-21 22:50:29', '2021-01-21 22:50:29', NULL, 'circle'),
(8, 'circle layer 1', '../storage/mapLayers/circle layer 1.json', '145', '2021-01-21 22:57:17', '2021-01-21 22:57:17', NULL, 'circle'),
(9, 'rectangles 1', '../storage/mapLayers/rectangles 1.json', '145', '2021-01-21 23:28:39', '2021-01-21 23:28:39', NULL, 'rectangle');

-- --------------------------------------------------------

--
-- Table structure for table `marker_dataset`
--

DROP TABLE IF EXISTS `marker_dataset`;
CREATE TABLE IF NOT EXISTS `marker_dataset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marker_dataset`
--

INSERT INTO `marker_dataset` (`id`, `srno`, `name`, `details`, `icon`, `lat`, `lng`, `color`, `size`, `dataset`, `created_at`, `updated_at`, `deleted_at`, `created_by`) VALUES
(1, '1', 'test1', 'detail1', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.1818371639955', '80.02127402195', NULL, NULL, 'dataset 1', '2021-01-21 00:07:52', '2021-01-21 00:07:52', NULL, '145'),
(2, '2', 'test2', 'details2', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.0878138341962', '80.096805027809', NULL, NULL, 'dataset 1', '2021-01-21 00:07:53', '2021-01-21 00:07:53', NULL, '145'),
(3, '3', 'test4', 'details4', NULL, '7.2336098438611', '80.069339207497', '#37DBCB', '5.8', 'dataset 1', '2021-01-21 00:07:53', '2021-01-21 00:07:53', NULL, '145'),
(4, '4', 'test4', 'details 4', '../storage/img/icons/anniversary.png', '6.9937713154954', '80.017154148903', '#37DBCB', '5.8', 'dataset 1', '2021-01-21 00:07:53', '2021-01-21 00:07:53', NULL, '145'),
(5, '1', 'ada', 'ada', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.1722994471156', '80.008914402809', NULL, NULL, 'dataset 1', '2021-01-21 01:35:48', '2021-01-21 01:35:48', NULL, '145'),
(6, 'a', 'a', 'a', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.1763870645442', '80.004794529762', NULL, NULL, 'sssssss', '2021-01-21 01:37:21', '2021-01-21 01:37:21', NULL, '145'),
(7, '1', 'aaa', 'aaa', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '6.9937713154954', '79.969088963356', NULL, NULL, 'dataset 2', '2021-01-21 01:50:19', '2021-01-21 01:50:19', NULL, '145'),
(8, '2', 'bbb', 'bbb', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.1668492334631', '80.150363377418', NULL, NULL, 'dataset 2', '2021-01-21 01:50:19', '2021-01-21 01:50:19', NULL, '145'),
(9, 'c', '333', '333', 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png', '7.2799277365981', '80.09955160984', NULL, NULL, 'dataset 2', '2021-01-21 01:50:19', '2021-01-21 01:50:19', NULL, '145'),
(10, '1', '111', '111', NULL, '7.2376969083734', '80.029513768043', '#A12E2E', '4.5', 'dataset 3', '2021-01-21 01:53:18', '2021-01-21 01:53:18', NULL, '145'),
(11, '2', '222', '2222', NULL, '7.1150689592152', '80.09955160984', '#A12E2E', '4.5', 'dataset 3', '2021-01-21 01:53:18', '2021-01-21 01:53:18', NULL, '145'),
(12, '3', '33', '333', '../storage/img/icons/gay-female.png', '7.1872871981222', '79.973208836403', '#A12E2E', '4.5', 'dataset 3', '2021-01-21 01:53:18', '2021-01-21 01:53:18', NULL, '145');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
